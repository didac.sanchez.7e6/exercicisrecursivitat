﻿using System;

namespace M03RecusivitatSanchezDidac
{
    class Recursivitat
    {
        static void Main(string[] args)
        {
            var menu = new Recursivitat();
            menu.Menu();
        }
        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.Exit");
                Console.WriteLine("1.Factorial");
                Console.WriteLine("2.Doble factorial");
                Console.WriteLine("3.Nombre de dígits");
                Console.WriteLine("4.Nombres creixents");
                Console.WriteLine("5.Reducció de dígits");
                Console.WriteLine("6.Seqüència d’asteriscos");
                Console.WriteLine("7.Primers perfectes");
                Console.WriteLine("8.Torres de Hanoi");
                opcio = Console.ReadLine();
                MenuOpcions(opcio);
            } while (opcio != "0");
        }
        public void MenuOpcions(string opcio)
        {
            switch (opcio)
            {
                case "0":
                    break;
                case "1":
                    Factorial();
                    break;
                case "2":
                    DobleFactorial();
                    break;
                case "3":
                    NombreDigits();
                    break;
                case "4":
                    NombresCreixents();
                    break;
                case "5":
                    ReduccioDigits();
                    break;
                case "6":
                    SequenciaAsteriscos();
                    break;
                case "7":
                    PrimersPerfectes();
                    break;
                case "8":
                    TorresHanoi();
                    break;
                default:
                    Console.Clear();
                    break;
            }

        }
        int AskNumero()
        {
            bool flag;
            int numero;
            do
            {
                string guess = (Console.ReadLine());
                flag = int.TryParse(guess, out numero);
            } while (!flag);
            return numero;
        }
        public void EndProgram()
        {
            Console.ReadLine();
            Console.Clear();
        }
        int doFactorial(int calcular, int salt)
        {
            if (calcular > salt) return calcular *= doFactorial(calcular - salt, salt);
            return 1;
        }
        public void Factorial()
        {
            Console.Clear();
            int factorial = AskNumero();
            factorial = doFactorial(factorial, 1);
            Console.WriteLine(factorial);
            EndProgram();
        }
        public void DobleFactorial()
        {
            Console.Clear();
            int factorial = AskNumero();
            factorial = doFactorial(factorial, 2);
            Console.WriteLine(factorial);
            EndProgram();
        }
        int CalcularLongitut(int numero)
        {
            int longitut = 1;
            if (numero / 10 > 0)
            {
                return longitut += CalcularLongitut((numero / 10));
            }
            return 1;
        }
        public void NombreDigits()
        {
            Console.Clear();
            int numeroCalcular = AskNumero();
            int longitut = CalcularLongitut(numeroCalcular);
            Console.WriteLine($"La longitut del numero {numeroCalcular} és {longitut}");
            EndProgram();
        }
        bool EsCreixent(bool creix, int num)
        {
            if (num > 9)
            {
                if (creix)
                {
                    creix = (num % 10 > num / 10 % 10);
                    return creix = EsCreixent(creix, num / 10);
                }
                if (!creix)
                {
                    return creix;
                }
            }
            return creix;
        }
        public void NombresCreixents()
        {
            Console.Clear();
            int numeroCalcular = AskNumero();
            bool flag = EsCreixent(true, numeroCalcular);
            Console.WriteLine($"Es {flag} que {numeroCalcular} es creixent");
            EndProgram();
        }
        int RaizDigital(int num)
        {
            if (num > 9)
            {
                return num = (num % 10) + RaizDigital(num / 10);

            }
            return num;
        }
        public void ReduccioDigits()
        {
            Console.Clear();
            int numeroCalcular = AskNumero();
            int reducio = numeroCalcular;
            do
            {
                reducio = RaizDigital(reducio);
            } while (reducio > 9);
            Console.WriteLine($"La raiz digital de {numeroCalcular} és {reducio}");
            EndProgram();
        }
        /* public void PrintPiramide(int alsadaMaxima, int curent = 1)
         {
             if (curent <= alsadaMaxima)
             {
                 for (int i = 0; i < curent; i++)
                 {
                     Console.Write("*");
                     if (i == curent-1)
                     {
                         Console.WriteLine("");
                     }

                 }
                 curent++;
                 PrintPiramide(alsadaMaxima, curent);
             }
         }
        public void PrintPiramide(int alsadaMaxima, int curent = 1, bool flag = true)
        {
            if (curent < alsadaMaxima)
            {
                if (flag)
                {
                    Console.WriteLine("*");
                    flag = !flag;
                    PrintPiramide(alsadaMaxima, curent, flag);
                    flag = !flag;
                }
                if (!flag)
                {
                    for (int i = 0; i <= curent; i++)
                    {
                        Console.Write("*");
                        if (i == curent) Console.WriteLine("");
                    }
                    flag = !flag;
                    curent++;
                    PrintPiramide(alsadaMaxima, curent, flag);
                    flag = !flag;
                }
            }
        }
        public void PrintInverPiramide(int alsadaMaxima, bool flag = true)
        {
            if (0 <= alsadaMaxima-2)
            {
                if (flag)
                {
                    Console.WriteLine("*");
                    flag = !flag;
                    PrintInverPiramide(alsadaMaxima, flag);
                    flag = !flag;
                }
                if (!flag && alsadaMaxima != 2)
                {
                    for (int i = alsadaMaxima-2; i >= 0; i--)
                    {
                        Console.Write("*");
                        if (i == 0) Console.WriteLine("");
                    }
                    flag = !flag;
                    alsadaMaxima--;
                    PrintInverPiramide(alsadaMaxima, flag);
                    flag = !flag;
                }
            }
        }*/
        static void PrintPiramide(int altura)
        {
            if (altura - 1 != 0)
            {
                PrintPiramide(altura - 1);
            }
            for(int i = 0; i < altura; i++)
            {
                Console.Write("*");
            }
            Console.WriteLine("");
            if (altura - 1 != 0)
            {
                PrintPiramide(altura - 1);
            }
        }
        public void SequenciaAsteriscos()
        {
            Console.Clear();
            int numeroCalcular = AskNumero();
            PrintPiramide(numeroCalcular);
            EndProgram();
        }
        bool EsPrime( int num, int contador = 1, bool flag = true)
        {
            contador++;
            if (flag && contador != num)
            {
                if (num % contador != 0)
                {
                    EsPrime(num, contador);
                    return flag;
                }
                else
                {
                    flag = false;
                    return flag;
                }
            }
            return flag;
        }
        public void PrimersPerfectes()
        {
            Console.Clear();
            int numeroCalcular = AskNumero();
            int reducio = numeroCalcular;
            bool flag = EsPrime(numeroCalcular);
            if (flag)
            {
                do
                {
                    reducio = RaizDigital(reducio);
                } while (reducio > 9);
                flag = EsPrime(reducio);
            }
            Console.WriteLine($"És {flag} que {numeroCalcular} és un primer perfectes");
            EndProgram();
        }
        public void TorresHanoi()
        {

        }

    }
}
